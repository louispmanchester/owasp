FROM tutum/lamp:latest

ENV DEBIAN_FRONTEND noninteractive

# Preparation
RUN rm -fr /app/* && \
  apt-get update && apt-get install -yqq wget unzip php5-curl dnsutils && \
  apt-get upgrade -yqq ca-certificates && \
  update-ca-certificates && \
  rm -rf /var/lib/apt/lists/*

# Deploy Mutillidae
RUN \
  wget -O /mutillidae.zip https://sourceforge.net/projects/mutillidae/files/latest/download && \
  unzip /mutillidae.zip && \
  rm -rf /app/* && \
  echo 'session.save_path = "/tmp"' >> /etc/php5/apache2/php.ini

RUN touch /mutillidae/owasp_installed
RUN cp -r /mutillidae/* /app

COPY start.sh /start.sh
RUN chmod +x /start.sh

EXPOSE 80 3306
CMD ["/start.sh"]