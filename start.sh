#!/bin/bash

if [ ! -f /app/owasp_installed ]; then
    echo "OWASP not installed, lets install it"
    cp -r /mutillidae/* /app
fi

/run.sh